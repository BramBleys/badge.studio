# How to Contribute

First of all thank you for considering contributing to badge studio. We are always very happy & thankfull if someone wants to help out & share things back to the community we want to build around badge studio.

#### Do I need to be a coder to contribute?

**No**, we welcome all kind of contributions.


To report an issue, help out with translations, report bugs or feature requests, correct content mistakes, write documentation ... no coding skills are required.

#### Gitlab Group, Repository & Gitter Chat channel

You can find the **Gitlab repository** of Badge Studio here: [https://gitlab.com/badgestudio/badge.studio](https://gitlab.com/badgestudio/badge.studio) .

If you want to contribute also to other **Badge Studio related projects** in the future we happily welcome you to become a member of the [Badge Studio Group on Gitlab](https://gitlab.com/badgestudio/) .

All communications we try to keep close to the code on Gitlab (at the [issues](https://gitlab.com/badgestudio/badge.studio/issues),the [merge requests](https://gitlab.com/badgestudio/badge.studio/merge_requests), the [wiki](https://gitlab.com/badgestudio/badge.studio/wikis/home) & via conversation threads that are linked to those)

There is also a **Gitter chat channel** with public lobby that can be used for direct messages & quick answers: [https://gitter.im/badgestudio](https://gitter.im/badgestudio)


## Guidelines

### Issue reporting

- You can find all issues [here](https://gitlab.com/badgestudio/badge.studio/issues) 
- Before you file an issue, make sure it doesn't yet exist
- Be as **clear** as possible in your title and description of the issue
- As with branches, we like our issue titles to be lowercase and seperated by dashes e.g **add-this-feature**
- Make sure your issue only tackles **one problem at a time**
- Try to **label** your issue appropriate & distinguish at least between Feature request (feature), bug (bug) & documentation
- Try to assign them to a **milestone**
- Only use the labels of the [issue board](https://gitlab.com/badgestudio/badge.studio/boards?=) : *To Do, In Review, Doing, Blocked* , if the issue is being worked on or has been selected to get worked on
- You may create new labels if you like, maintainers of the repo will do clean-ups when necessary

### Merge Requests

- The `develop` branch is the default branch to checkout from and to merge back against.
- The `master` branch is a protected, production branch and reflects the version that is live on [https://badge.studio](https://badge.studio)
#### Want to **start working on an issue** ? 
- Our preferred workflow is the default one of Gitlab, available through the Gitlab UI via the button **Create merge request** on the page of an issue:
        
    - make sure to always start from an issue, please file one first if it doesn't exist yet
    - Branch from `develop`
    - name your branch starting with the number of the issue you are working on & the name of the issue e.g. `15-add-feature-y`
    - we like our branches (as our issues) to be lowercase and seperated by dashes
    - you may push as many commits as needed in your branch (you can squash them before merge)
    - work in the src folder and don't push the `dist` in the commits (.gitignore leaves it out anyway)
    - by Gitlab convention: we like you to already file a merge request during the time you are working on it, but make sure to add **WIP** before your title so it will not be accidently merged: e.g.`WIP: Resolve "name-of-the-issue"`
    - Always merge back against the `develop` branch
    - Best practice is to request a review before you actually merge, so someone else can adjust/approve/comment, label the issue also 'In review'
    - When approved you may merge to the `develop` branch
    - **clean up** after you merge by removing the source branch that you merged from & **closing the issue** (which Gitlab should do automatically if you followed guidelines)

### Deployment pipeline

- Via .gitlab-ci.yml a CI/CD pipeline is triggered when anything is merged into the `master` branch , this will automatically build & deploy the app to [https://badge.studio](https://badge.studio) via  [Google Firebase](http://firebase.google.com).
- Only maintainers can merge into the master branch
- We are planning to daily merge the `develop` branch into `master` to keep the app up to date as long as there are no problems that pop up.


### Development Setup

The app is made with [Quasar-framework](https://quasar-framework.org/) & [Firebase](https://firebase.google.com) .
You need to have **vue-cli** & **quasar** globally installed + **Node.js >= 8.9.0** is required along **Yarn** or **NPM**.

``` bash
# make sure you have vue-cli & quasar globally installed + Node.js >= 8.9.0 is required.
 
$ 'yarn global add vue-cli' (or 'npm install -g vue-cli')
$ 'yarn global add quasar-cli' (or 'npm install -g quasar-cli')
 
# install package dependencies
 
$ 'yarn install' (or 'npm install')
 
# serve with hot reload for development
 
$ 'quasar dev' (or 'quasar dev -m pwa' when developing a PWA)
 
# build for production
 
$ 'quasar build' (or 'quasar build -m pwa' when PWA mode)
 
```

### How to get in touch

If you have any questions or want to get in touch, do join our [Gitter channel](https://gitter.im/badgestudio) or [email us](mailto:bert@badge.studio).