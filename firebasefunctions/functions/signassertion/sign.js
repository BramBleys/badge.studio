import jws from "jws"
import fs from "fs"
import bakery from "openbadges-bakery"
import { https } from "firebase-functions"

const theassertion = {
  "uid": "abcdefghijklm1234567898765",
  "recipient": {
    "identity": "sha256$a1b2c3d4e5f6g7h8i9a1b2c3d4e5f6g7h8i9a1b2c3d4e5f6g7h8i9",
	"type": "email",
	"hashed": true
  },
  "badge": "http://issuersite.org/badge-class.json",
  "verify": {
    "url": "http://issuersite.org/public-key.pem",
	"type": "signed"
   },
   "issuedOn": 1403120715
}

const signature = jws.sign({
  header: {alg: 'rs256'},
  payload: theassertion,
  privateKey: fs.readFileSync(__dirname + '/private-key.pem')
})

const img = fs.readFileSync(__dirname + '/badge.png')

const options ={
  image: img,
  assertion: signature
}
exports.signbadge = https.onRequest((request, response) => {
bakery.bake (options, function(err, data){
  if(err) response.send(err);
  else {
		//give the baked badge a file name
		const fileName = 'bakedbadge.png';
		const imagePath = __dirname+"/baked/"+fileName;//"baked" directory
		//write the returned baked badge data to file
		fs.writeFile(imagePath, data, function (err) {
			if(err) response.send(err);
			else response.send("<img src='"+imagePath+"' alt='badge'/>");
		});
  }
});
});


