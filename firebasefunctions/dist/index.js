"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.api = void 0;

var functions = _interopRequireWildcard(require("firebase-functions"));

var _server = _interopRequireDefault(require("./graphql/server"));

var _firebaseAdmin = _interopRequireDefault(require("firebase-admin"));

var _jws = _interopRequireDefault(require("jws"));

var _fs = _interopRequireDefault(require("fs"));

var _openbadgesBakery = _interopRequireDefault(require("openbadges-bakery"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

_firebaseAdmin.default.initializeApp(functions.config().firebase);
/* CF for Firebase with graphql-server-express */


const graphQLServer = (0, _server.default)(); // https://us-central1-<project-name>.cloudfunctions.net/api

const api = functions.https.onRequest(graphQLServer);
exports.api = api;
const theassertion = {
  "uid": "abcdefghijklm1234567898765",
  "recipient": {
    "identity": "sha256$a1b2c3d4e5f6g7h8i9a1b2c3d4e5f6g7h8i9a1b2c3d4e5f6g7h8i9",
    "type": "email",
    "hashed": true
  },
  "badge": "http://issuersite.org/badge-class.json",
  "verify": {
    "url": "http://issuersite.org/public-key.pem",
    "type": "signed"
  },
  "issuedOn": 1403120715
};

const signature = _jws.default.sign({
  header: {
    alg: 'rs256'
  },
  payload: theassertion,
  privateKey: _fs.default.readFileSync(__dirname + '/private-key.pem')
});

exports.sign = functions.https.onRequest((request, response) => {
  response.send(signature);
});
exports.assertions = functions.https.onRequest((request, response) => {
  response.send(_firebaseAdmin.default.firestore().collection('assertions').doc('test').get().then(doc => {
    console.log('Got rule: ' + doc.data().name);
  }));
});