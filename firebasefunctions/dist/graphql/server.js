"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _express = _interopRequireDefault(require("express"));

var _graphqlServerExpress = require("graphql-server-express");

var _schema = _interopRequireDefault(require("./data/schema"));

var _schemaPrinter = require("graphql/utilities/schemaPrinter");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function setupGraphQLServer() {
  const graphQLServer = (0, _express.default)(); // /api/graphql

  graphQLServer.use("/graphql", _bodyParser.default.json(), (0, _graphqlServerExpress.graphqlExpress)({
    schema: _schema.default,
    context: {}
  })); // /api/graphiql

  graphQLServer.use("/graphiql", (0, _graphqlServerExpress.graphiqlExpress)({
    endpointURL: "/api/graphql"
  })); // /api/schema

  graphQLServer.use("/schema", (req, res) => {
    res.set("Content-Type", "text/plain");
    res.send((0, _schemaPrinter.printSchema)(_schema.default));
  });
  return graphQLServer;
}

var _default = setupGraphQLServer;
exports.default = _default;