"use strict";

var _jws = _interopRequireDefault(require("jws"));

var _fs = _interopRequireDefault(require("fs"));

var _openbadgesBakery = _interopRequireDefault(require("openbadges-bakery"));

var _firebaseFunctions = require("firebase-functions");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const theassertion = {
  "uid": "abcdefghijklm1234567898765",
  "recipient": {
    "identity": "sha256$a1b2c3d4e5f6g7h8i9a1b2c3d4e5f6g7h8i9a1b2c3d4e5f6g7h8i9",
    "type": "email",
    "hashed": true
  },
  "badge": "http://issuersite.org/badge-class.json",
  "verify": {
    "url": "http://issuersite.org/public-key.pem",
    "type": "signed"
  },
  "issuedOn": 1403120715
};

const signature = _jws.default.sign({
  header: {
    alg: 'rs256'
  },
  payload: theassertion,
  privateKey: _fs.default.readFileSync(__dirname + '/private-key.pem')
});

const img = _fs.default.readFileSync(__dirname + '/badge.png');

const options = {
  image: img,
  assertion: signature
};
exports.signbadge = _firebaseFunctions.https.onRequest((request, response) => {
  _openbadgesBakery.default.bake(options, function (err, data) {
    if (err) response.send(err);else {
      //give the baked badge a file name
      const fileName = 'bakedbadge.png';
      const imagePath = __dirname + "/baked/" + fileName; //"baked" directory
      //write the returned baked badge data to file

      _fs.default.writeFile(imagePath, data, function (err) {
        if (err) response.send(err);else response.send("<img src='" + imagePath + "' alt='badge'/>");
      });
    }
  });
});