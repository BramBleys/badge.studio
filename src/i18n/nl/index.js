export default {
  landing: {
    intro: {
      top: 'Maak de badges die je krijgt FAIR',
      title: 'Badge Studio',
      about: 'Over het project',
      create: 'Maak een badge',
      login: 'Sign in'
    }
  },
  stepper: {
    next: 'Volgende',
    back: 'Terug',
    card: {
      framework: 'Competentiekader',
      certificate: 'Certificaat van voltooiing'
    },
    step1: {
      step_title: 'Kies onderschrijving',
      title: 'Kies een onderschrijving',
      select: 'Kies competentiekader',
      search: 'Type een vaardigheid',
      selectOptions: [
        {
          label: 'Geen competentiekader',
          value: 'none'
        },
        {
          label: 'ESCO',
          value: 'esco'
        }
      ]
    },
    step3: {
      step_title: 'Voeg details toe',
      title: 'Voeg details toe',
      authenticate: 'Gebruik geverifieerde identiteit',
      name: 'Je naam of organisatie',
      email: 'Je e-mail',
      criteria: 'Criteria',
      how: 'Hoe is deze badge verdiend?',
      link: 'Jouw website (optioneel)'
    },
    step4: {
      step_title: 'Ken de badge toe',
      title: 'Ken de badge toe (optioneel)',
      toggle: 'Laat de ontvanger de badge zelf claimen',
      name: 'Naam van de ontvanger',
      email: 'E-mail van de ontvanger',
      why: 'Waarom verdient hij/zij deze badge'
    },
    step5: {
      step_title: 'Overzicht & deel',
      title: 'Deel je badge',
      issuer: 'Uitgever badge',
      receiver: 'Ontvanger badge',
      description: 'Omschrijving',
      explanation: 'Toelichting',
      download: 'Download',
      email: 'Verstuur via e-mail'
    }
  }
}
