import nlMessages from '../nl'

export default {
  'en-us': {
    landing: {
      intro: {
        top: 'Make the badges you receive FAIR',
        title: 'Badge Studio',
        about: 'About the project',
        create: 'Create a badge',
        login: 'Sign in'
      }
    },
    stepper: {
      next: 'Next',
      back: 'Back',
      card: {
        framework: 'Competency framework',
        certificate: 'Certificate of completion'
      },
      step1: {
        step_title: 'Choose endorsement',
        title: 'Choose an endorsement',
        select: 'Choose competency framework',
        search: 'Type a skill',
        selectOptions: [
          {
            label: 'No competency framework',
            value: 'none'
          },
          {
            label: 'ESCO',
            value: 'esco'
          }
        ]
      },
      step3: {
        step_title: 'Add your details',
        title: 'Add your details',
        authenticate: 'Use verified identity',
        name: 'Your name or organisation',
        email: 'Your e-mail',
        criteria: 'Criteria',
        how: 'How was this badge earned?',
        link: 'Your website (optional)'
      },
      step4: {
        step_title: 'Award the badge',
        title: 'Award the badge (optional)',
        toggle: 'The receiver can claim the badge',
        name: 'Name of the receiver',
        email: 'E-mail of the receiver',
        why: 'Why does he/she earn this badge?'
      },
      step5: {
        step_title: 'Overview & share',
        title: 'Share your badge',
        issuer: 'Issuer badge',
        receiver: 'Receiver badge',
        description: 'Description',
        explanation: 'Explanation',
        download: 'Download',
        email: 'Send via e-mail'
      }
    }
  },
  nl: nlMessages
}
